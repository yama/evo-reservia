<?php
class reservia {
    
    public $end_point;
    public $application_token;
    public $seed;
    public $shop_id;
    public $path;
    
    public $option = array();
    
    private $api = array();
    private $field_name = array();
    private $res_count = array();
    
    function __construct() {
        if(!is_file(__DIR__.'/config.inc.php')) exit('config.inc.phpがありません。');
        else                                    include(__DIR__.'/config.inc.php');
        
        include(__DIR__.'/api.info.inc.php');
        $this->api = $_;
        $this->field_name = $field_name;
        
        if(isset($this->application_token)) $this->setOption('application_token',$this->application_token);
        if(isset($this->seed))              $this->setOption('seed',$this->seed);
        
        $this->end_point   = 'https://api.reservia.jp';
        $this->default_path = '/shop/details';
    }
    
    public function getApiRespons() {
        
        if(!$this->getOption('seed'))              return 'seedをセットしてください。';
        if(!$this->getOption('application_token')) return 'application_tokenをセットしてください。';
        
        $datetime = date('YmdHis');
        $this->setOption('datetime',          $datetime);
        $this->setOption('access_token',      md5($datetime.$this->getOption('seed')));
        if($this->getOption('page') && 1<$this->getOption('page')) {
            $this->setOption('page_number', $this->getOption('page'));
        }
        
        static $respons = array();
        
        $cacheKey = md5(print_r($options,true));
        if(isset($respons[$cacheKey])) return $respons[$cacheKey];
        
        $options = array(
            CURLOPT_URL => $this->end_point.$this->option['path'],
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POSTFIELDS  => http_build_query($this->option),
        );
        
        $ch=curl_init();
        curl_setopt_array($ch, $options);
        $respons[$cacheKey] = json_decode(curl_exec($ch), true);
        return $respons[$cacheKey];
    }
    
    public function setDefaultOption($options= array()) {
        
        if(!isset($options['path'])) $options['path'] = $this->default_path;
        $options['path'] = '/'.trim($options['path'],'/');
        
        if(isset($options['limit'])) $options['page_size']   = $options['limit'];
        if(isset($options['page']))  $options['page_number'] = $options['page'];
        
        $this->option = array_merge($this->option,$options);

        if(!$this->getOption('shop_id')) $this->setOption('shop_id', $this->default_shop_id);
    }
    
    public function setOption($key,$value) {
        $this->option[$key] = $value;
    }
    
    public function getOption($key,$default='') {
        if(isset($this->option[$key])) return $this->option[$key];
        return $default;
    }
    
    public function forgeRespons($respons) {
        
        if(!$respons) return false;
        
        $path = $this->getOption('path');
        $field_name = $this->field_name[$path]['result'];
        $rs = $respons[$field_name];
        $offset = $this->getOption('offset',0);
        foreach((array)$rs as $i=>$row) {
            if(!is_array($rs[0])) break;
            $rs[$i]['i'] = $i+$offset+1;
        }
        return $rs;
    }
    
    private function getTotalofReviewList($evaluation_count) {
        $evaluations = $evaluation_count;
        $total = 0;
        if($evaluations) {
            foreach($evaluations as $ev) {
                $total += $ev['count'];
            }
        }
        return $total;
    }
    
    public function getStaffIds() {
        $this->setOption('path','/staff/list');
        $rs = $this->getApiRespons();
        print_r($rs);exit;
        $_ = $this->forgeRespons($rs);
        $staff_ids = array();
        foreach($_['result'] as $row) {
            $staff_ids[] = $row['id'];
        }
        return $staff_ids;
    }
}
