<?php

include_once(__DIR__.'/api/reservia.class.inc.php');

class evo_reservia extends reservia {
    
    private $conf;
    
    function __construct() {
        global $modx;
        
        parent::__construct();
        
        $this->conf = $modx->event->params;
    }
    
    function run() {
        global $modx;
        
        extract($this->conf);
        
        if(!isset($this->conf['id']))              $this->conf['id'] = '';
        
        if(!isset($this->conf['tplPages']))        $this->conf['tplPages']        = '<a href="[+url+]">[+page+]</a>';
        if(!isset($this->conf['tplPagesCurrent'])) $this->conf['tplPagesCurrent'] = '[+page+]';
        if(!isset($this->conf['tplPagesSep']))     $this->conf['tplPagesSep']     = ' | ';
        
        if($_GET) $option = array_merge($this->conf,$_GET);
        else      $option = $this->conf;
        
        if(isset($option[$id.'.page'])) {
            $option['page'] = $option[$id.'.page'];
            unset($option[$id.'.page']);
        }
        
        parent::setDefaultOption($option);
        
        if(isset($application_token)) parent::setOption('application_token', $application_token);
        if(isset($seed))              parent::setOption('seed', $seed);
        
        if(isset($_GET['info'])) {
        //    $staff_ids = parent::getStaffIds();
            $content = file_get_contents(__DIR__.'/api/info.html');
            exit($content);
        }
        if(!isset($tpl)) $tpl = '';
        
        if(isset($_GET['path'])) $tpl = '';
        
        elseif(strpos($tpl,'[+')===false) $tpl = $modx->getChunk($tpl);
        $res = parent::getApiRespons();
        
        if(!$res) return '1';
        
        if(isset($_GET['json'])) {
            return '<pre>'.print_r($res,true).'</pre>';
        }
        
        $this->setPlaceholders($res);
        
        $rs = parent::forgeRespons($res);
        
        if(!$tpl) {
            return 'テンプレート(&tpl)を指定してください。プレースホルダ名と取得できる値は下記のとおりです。<pre>'.print_r($rs,true).'</pre>';
        }
        
        $path = parent::getOption('path','/shop/details');
        if(parent::getOption('mode','')) {
            $ph = $rs;
            return $modx->parseText($tpl,$ph);
        }
        elseif(strpos($path,'/list')!==false) {
            $_ = array();
            foreach((array)$rs as $ph) {
                $_[] = $modx->parseText($tpl,$ph);
            }
            return join("\n", $_);
        }
        elseif(strpos($path,'/details')!==false) {
            $ph = $rs;
            return $modx->parseText($tpl,$ph);
        }
    }
    
    function setPlaceholders($res) {
        global $modx;
        
        $id = $this->conf['id'];
        
        if(isset($res['number_of_pages'])) {
            $lastPage = $res['number_of_pages'];
            $currentPage = !isset($_GET['page']) ? 1 : $_GET['page'];
            if($currentPage==1) {
                $prev = 0;
                $next = 2;
            }
            else {
                $prev = $_GET['page']-1;
                $next = $_GET['page']+1;
            }
            if(!$prev)            $prev = '';
            if($lastPage < $next) $next = '';
            if(isset($_GET)) {
                $g = $_GET;
                ksort($g);
                $pageKey = $id . '.page';
            }
            $modx->setPlaceholder($id.'.prevPage',$prev);
            $modx->setPlaceholder($id.'.nextPage',$next);
            $modx->setPlaceholder($id.'.lastPage',$lastPage);
            $pages = $this->getPages($currentPage,$lastPage);
            $modx->setPlaceholder($id.'.Pages',$pages);
            $modx->setPlaceholder($id.'.currentPage',$currentPage);
            $modx->setPlaceholder($id.'.total_count@/review/list',$res['total_count']);
            parent::setOption('offset', ($currentPage-1) * 10);
        }
    }
    
    function getPages($currentPage,$lastPage) {
        global $modx;
        $pages = array();
        for($i=1;$i<=$lastPage;$i++) {
            $ph['page'] = $i;
            if($i==$currentPage) $pages[] = $modx->parseText($this->conf['tplPagesCurrent'], $ph);
            else                 $pages[] = $modx->parseText($this->conf['tplPages'], $ph);
        }
        return join($this->conf['tplPagesSep'], $pages);
    }
}

