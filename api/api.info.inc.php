<?php
$field_name['/shop/details']['name']              = '店舗詳細';
$field_name['/staff/list']['name']                = 'スタッフ一覧';
$field_name['/staff/details']['name']             = 'スタッフ詳細';
$field_name['/review/list']['name']               = '口コミ一覧';
$field_name['/review/details']['name']            = '口コミ詳細';
$field_name['/review_theme/list']['name']         = '口コミテーマ一覧';
$field_name['/coupon/list']['name']               = 'クーポン一覧';
$field_name['/coupon/details']['name']            = 'クーポン詳細';
$field_name['/coupon/create']['name']             = 'クーポン登録';
$field_name['/coupon/edit']['name']               = 'クーポン編集';
$field_name['/coupon/delete']['name']             = 'クーポン削除';
$field_name['/coupon_type/list']['name']          = 'クーポンカテゴリ一覧';
$field_name['/menu/list']['name']                 = 'メニュー一覧';
$field_name['/rank/list']['name']                 = 'ランク一覧';
$field_name['/reservation/availability']['name']  = '予約空き状況';
$field_name['/reservation/create']['name']        = '予約登録';
$field_name['/reservation/cancel']['name']        = '予約キャンセル';
$field_name['/reservation/updatehistory']['name'] = '予約更新履歴';
$field_name['/staff/authenticate']['name']        = 'スタッフ認証';
$field_name['/user/details']['name']              = '会員情報詳細';
$field_name['/user/id']['name']                   = '会員ID取得';
$field_name['/user/login']['name']                = 'ログイン用URL一覧';

$field_name['/shop/details']['result']              = 'shop';
$field_name['/staff/list']['result']                = 'staffs';
$field_name['/staff/details']['result']             = 'staff';
$field_name['/review/list']['result']               = 'reviews';
$field_name['/review/details']['result']            = 'review';
$field_name['/review_theme/list']['result']         = 'review_themes';
$field_name['/coupon/list']['result']               = 'coupons';
$field_name['/coupon/details']['result']            = 'coupon';
$field_name['/coupon/create']['result']             = 'coupon_id';
$field_name['/coupon/edit']['result']               = 'validation_errors';
$field_name['/coupon/delete']['result']             = '';
$field_name['/coupon_type/list']['result']          = 'coupon_types';
$field_name['/menu/list']['result']                 = 'menus';
$field_name['/rank/list']['result']                 = 'ranks';
$field_name['/reservation/availability']['result']  = 'date_items';
$field_name['/reservation/create']['result']        = 'reservation_id';
$field_name['/reservation/cancel']['result']        = '';
$field_name['/reservation/updatehistory']['result'] = 'update_history';
$field_name['/staff/authenticate']['result']        = 'staff';
$field_name['/user/details']['result']              = 'user';
$field_name['/user/id']['result']                   = 'user_id';
$field_name['/user/login']['result']                = 'login_urls';

$field_name['/shop/details']['meta']              = '';
$field_name['/staff/list']['meta']                = '';
$field_name['/staff/details']['meta']             = '';
$field_name['/review/list']['meta']               = 'evaluation_count';
$field_name['/review/details']['meta']            = '';
$field_name['/review_theme/list']['meta']         = '';
$field_name['/coupon/list']['meta']               = '';
$field_name['/coupon/details']['meta']            = '';
$field_name['/coupon/create']['meta']             = '';
$field_name['/coupon/edit']['meta']               = '';
$field_name['/coupon/delete']['meta']             = '';
$field_name['/coupon_type/list']['meta']          = '';
$field_name['/menu/list']['meta']                 = '';
$field_name['/rank/list']['meta']                 = '';
$field_name['/reservation/availability']['meta']  = '';
$field_name['/reservation/create']['meta']        = '';
$field_name['/reservation/cancel']['meta']        = '';
$field_name['/reservation/updatehistory']['meta'] = 'total_count';
$field_name['/staff/authenticate']['meta']        = '';
$field_name['/user/details']['meta']              = '';
$field_name['/user/id']['meta']                   = '';
$field_name['/user/login']['meta']                = '';

$field_name['/review/list']['meta']               = 'evaluation_count';
$field_name['/reservation/updatehistory']['meta'] = 'total_count';

return $field_name;

/*
$field_name['/shop/details']['name']              = '店舗詳細',        
$field_name['/staff/list']['name']                = 'スタッフ一覧',    
$field_name['/staff/details']['name']             = 'スタッフ詳細',    
$field_name['/review/list']['name']               = '口コミ一覧',      ;
$field_name['/review/details']['name']            = '口コミ詳細',      
$field_name['/review_theme/list']['name']         = '口コミテーマ一覧',
$field_name['/coupon/list']['name']               = 'クーポン一覧',    
$field_name['/coupon/details']['name']            = 'クーポン詳細',    
$field_name['/coupon/create']['name']             = 'クーポン登録',    
$field_name['/coupon/edit']['name']               = 'クーポン編集',    
$field_name['/coupon/delete']['name']             = 'クーポン削除',    
$field_name['/coupon_type/list']['name']          = 'クーポンカテゴリ一覧','req_key'=>'shop_id');
$field_name['/menu/list']['name']                 = 'メニュー一覧',    'req_key'=>'shop_id,staff_id,menu_kind','req_subkey'=>'menu_kind');
$field_name['/rank/list']['name']                 = 'ランク一覧',      'req_key'=>'shop_id');
$field_name['/reservation/availability']['name']  = '予約空き状況',    'req_key'=>'shop_id,staff_id,menu_id,start_date,end_date');
$field_name['/reservation/create']['name']        = '予約登録',        'req_key'=>'shop_id,staff_id,menu_id,start_time,user_name,user_email,user_tel');
$field_name['/reservation/cancel']['name']        = '予約キャンセル',    'req_key'=>'shop_id,reservation_id','res_key'=>'');
$field_name['/reservation/updatehistory']['name'] = '予約更新履歴',    'req_key'=>'shop_id,page_number,updated_from,updated_to','res_count'=>'total_count');
$field_name['/staff/authenticate']['name']        = 'スタッフ認証',     'req_key'=>'login_id,password');
$field_name['/user/details']['name']              = '会員情報詳細',     'req_key'=>'user_id');
$field_name['/user/id']['name']                   = '会員ID取得',       'req_key'=>'user_access_key,user_seed');
$field_name['/user/login']['name']                = 'ログイン用URL一覧','req_key'=>'shop_id,redirect_url');
*/